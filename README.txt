This module provides a contact form that allows users to send an email message to the author of a node without knowing the username or user id of the author. Neither the contact form nor the "Send myself a copy" emails will reveal the name or UID of the node's author. The contact form is accessed at contact_anon/[nid] .

The module returns drupal_not_found() If the current user does not have permission to view the node specified in the url or the author of that node has chosen not to receive messages via the anonymous contact form.

Note: Aside from the contact form that this module provides, this module does not do anything to hide the username or UID of a node's author. It is up to the developer\administrator\designer of the site to make sure that this information is not revealed in places such as the node template or in any Views.
